import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersListComponent } from './users-list/users-list.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardService } from './dashboard.service';
import { CreateUserComponent } from './create-user/create-user.component';
import { FormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAdvertisementComponent } from './user-advertisement/user-advertisement.component';
import { AuthGuard } from '../auth.guard';
import { SearchOnTablePipe } from './search-on-table.pipe';


@NgModule({
  declarations: [DashboardComponent, UsersListComponent, CreateUserComponent, UserEditComponent, UserAdvertisementComponent, SearchOnTablePipe],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule
  ],
  providers: [DashboardService, AuthGuard]
})
export class DashboardModule { }
