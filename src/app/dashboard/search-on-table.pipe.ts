import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchOnTable'
})
export class SearchOnTablePipe implements PipeTransform {

  transform(value: any, args: string): any {
    let usersList = value.filter((user) => user.username.toLowerCase().includes(args.toLocaleLowerCase()));
    return usersList;
  }

}
