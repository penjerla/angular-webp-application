import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { HomeComponent } from '../home/home.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UsersListComponent } from './users-list/users-list.component';

const routes: Routes = [{ 
  path:"", component: DashboardComponent,
  children: [
    {path:"home", component: HomeComponent},
    {path:"users", component: UsersListComponent},
    {path:"createUser", component: CreateUserComponent, canActivate: [AuthGuard]},
    {path:"userEdit/:id", component: UserEditComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
