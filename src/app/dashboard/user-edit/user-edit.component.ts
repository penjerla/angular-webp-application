import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public updateUserObj = {
    'firstName': '',
    'lastName': '',
    'phone': '',
    'username': '',
    'password': ''
  }
  public userId: string;
  constructor(private dashboardService: DashboardService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    //this.userId = localStorage.getItem('userId');
    this.userId = this.route.snapshot.params['id'];
    console.log(this.userId);
    this.getUserDetailsById();
  }

  getUserDetailsById(){
    this.dashboardService.getUserDetails(this.userId).subscribe((resp: any) => {
      console.log(resp);
      if(resp.success) {
        this.updateUserObj = resp.metadata;
      }
    })
  }

  onUpdate(){
    this.dashboardService.updateUserDetails(this.updateUserObj).subscribe((resp: any) => {
      if(resp.success) {
        alert(resp.msg);
      }else {
        alert(resp.msg);
      }
    })
  }

}
