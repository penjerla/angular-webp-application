import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../global.config';

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient) { }

  getUsersList(){
    return  this.http.get(`${GlobalConstants.apiURL}/usersList`);
  }
  createUser(createUserObj){
    return this.http.post(`${GlobalConstants.apiURL}/userCreation`, createUserObj);
  }
  deleteUser(id){
    return this.http.get(`${GlobalConstants.apiURL}/deleteUser/${id}`);
  }

  getUserDetails(id){
    return this.http.get(`${GlobalConstants.apiURL}/fetchUserData/${id}`);
  }

  updateUserDetails(updateUserObj){
    
    return this.http.post(`${GlobalConstants.apiURL}/updateProfile`, updateUserObj);
  }

  validateLogin(obj) {
    return this.http.post(`${GlobalConstants.apiURL}/login`, obj);
  }
}
