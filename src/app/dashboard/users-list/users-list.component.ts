import { Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, map } from 'rxjs/operators'; 
import { UserAdvertisementComponent } from '../user-advertisement/user-advertisement.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit, OnDestroy, AfterViewInit {
  public usersList: any = [];
  public originalUsersList:any = [];
  public childData: string = '';
  public displayDate: string = "";
  public searchText: string = "";
  public interval:any;
  public v=0;
  public loginObj = JSON.parse(localStorage.getItem('loginInfo'));
  public getUserName  = this.loginObj.userName;
  public allowFlagData = false;
  public parentObj = {
    "name": "sandeep",
    "lastName": "penjerla"
  }

  private currentDate;
  private date;
  private month;
  private year;
  @ViewChild('searchRef') searchElementRef : ElementRef;
  @ViewChild(UserAdvertisementComponent) childElementRef : UserAdvertisementComponent;
  constructor(private dashboardService: DashboardService, private router: Router) { }

  ngAfterViewInit(): void {
    this.searchElementRef.nativeElement.focus();
    this.childElementRef.welcomeText = 'Hello';
  }
  ngOnInit(): void {
    this.getUsers();
    this.currentDate = new Date();
    this.date = this.currentDate.getDate();
    this.month = this.currentDate.getMonth() + 1;
    this.year = this.currentDate.getFullYear();
    this.displayDate = `${this.date} - ${this.month}  - ${this.year}`;

    let sample = [2,3,6,79,56,67,10];
    //console.log(sample);
    let filteredData = sample.filter((val) => val%2 == 0);
    //console.log(filteredData);

    let mapData = sample.map((val) => val*2);
    //console.log(mapData);


    //console.log('sandeep');

    // setTimeout(() => {
    //   console.log('sandeep1');
    // }, 1000);

    // this.interval = setInterval(() => {
    //   console.log('sandy');
    // }, 1000);

    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve('data');
        resolve('data5');
        resolve('data6');
      }, 1000);
    });
    promise.then(result => {
      //console.log("promise", result);
    });

    const observable:any = new Observable(observar=> {
      let i=0;
      setTimeout(() => {
        observar.next(i);
        i++;
      }, 100);
    });
   
    observable.subscribe(result => {
      //console.log("observable", result);
    });

    observable.subscribe(result => {
      //console.log("observable", result);
    });
    observable.subscribe(result => {
      //console.log("observable", result);
    });
    observable.subscribe(result => {
      //console.log("observable", result);
    });
    observable.subscribe(result => {
     // console.log("observable", result);
    });
    observable.subscribe(result => {
     // console.log("observable", result);
    });
    observable.subscribe(result => {
      //console.log("observable", result);
    });

    const subject:any = new Subject();
    subject.subscribe(result => {
      //console.log("subject", result);
      this.v++;
    })
    subject.subscribe(result => {
      //console.log("subject", result);
    })
    

    
    subject.next(this.v);

  }

  getUsers() {
    this.dashboardService.getUsersList().subscribe((resp: any) => {
      if (resp.success) {
        this.usersList = resp.metadata;
        this.originalUsersList = this.usersList;

        //console.log(this.usersList);
      } else {
        alert('Fetching users list is failed');
      }
    })
  }

  // async getUsers() {
  //   let resp:any = await this.dashboardService.getUsersList().toPromise();
  //   if (resp.success) {
  //     this.usersList = resp.metadata;
  //     this.originalUsersList = this.usersList;

  //     //console.log(this.usersList);
  //   } else {
  //     alert('Fetching users list is failed');
  //   }
  // }

  searchData(){
    console.log(this.searchText);
    this.usersList = this.originalUsersList;
    this.usersList = this.usersList.filter((user) => user.username.toLowerCase().includes(this.searchText));
  }

  onDelete(user: any) {
    let popup = confirm(`Are you sure you want to delete - ${user.username}`);
    //console.log(popup);
    if (popup) {
      this.dashboardService.deleteUser(user.user_id).subscribe((resp: any) => {
        if (resp.success) {
          this.getUsers();
          alert(resp.msg);
        } else {
          alert(resp.msg);
        }
      },(error) => {
        console.log(error);
      })
    }
  }

  onEdit(uid:string){
    //console.log(uid);
    localStorage.setItem("userId", uid);
    this.router.navigate(['/dashboard/userEdit', uid]);
  }

  onSubscribe(event){
    //console.log(event);
    this.childData = event;
  }

  validateDate(time): boolean {
    //console.log(time);
    let splitDate = time.split('T');
    //console.log('split Date', splitDate);
    const recordDate:any = new Date(splitDate[0]);
    const currentDate:any = new Date();
    const diffTime: any = Math.abs(currentDate - recordDate);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    if(diffDays>=7) {
      return true;
    }
    return false;
  }

  ngOnDestroy(){
    //this.interval.clearInterval();
  }

}
