import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  public createUserObj = {
    'firstName': '',
    'lastName': '',
    'phone': '',
    'username': '',
    'password': ''

  }
  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.createUserObj);
    this.dashboardService.createUser(this.createUserObj).subscribe((resp: any) => {
      if(resp.success) {
        //this.clearUserObj();
        alert(resp.msg);
      }else {
       // this.clearUserObj();
        alert(resp.msg);
      }
      this.clearUserObj();

    })

  }

  clearUserObj(){
    this.createUserObj = {
      'firstName': '',
      'lastName': '',
      'phone': '',
      'username': '',
      'password': ''
    }
  }

}
