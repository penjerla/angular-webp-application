import { Component, OnInit, Input, Output, EventEmitter, DoCheck, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-user-advertisement',
  templateUrl: './user-advertisement.component.html',
  styleUrls: ['./user-advertisement.component.css']
})
export class UserAdvertisementComponent implements OnInit, OnChanges, DoCheck {
  @Input() public currentPage;
  @Input () public allowFlag?;
  @Input () public parentObjDetails?;
  @Output() public subscribeEvent: EventEmitter<any> = new EventEmitter<any>();
  public myName: string = 'sandeep';
  public welcomeText = "Hi Welcome to my tutorial";

  public showSubscribeButton: boolean = true;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Onchanges', changes);
    console.log('onChangesFlag', this.allowFlag);
    console.log('onChangesParentObj', this.parentObjDetails);
  }

  ngDoCheck(): void {
    console.log('docheckFlag', this.allowFlag);
    console.log('docheckParentObj', this.parentObjDetails);
  }

  ngOnInit(): void {
    console.log('init', this.allowFlag);
  }

  onSubscriptionClick(){
    this.showSubscribeButton = false;
    this.subscribeEvent.emit('subscribed');
  }

  onAlertMessage() {
    alert(`Hi ${this.myName}`);
  }

}
