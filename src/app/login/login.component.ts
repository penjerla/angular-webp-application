import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../dashboard/dashboard.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginObj = {userName: '', password:''};
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  constructor( private router: Router, private dashboardService: DashboardService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.maxLength(5)]],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    localStorage.setItem("loginInfo", JSON.stringify(this.loginObj));
    this.router.navigate(['/dashboard']);
    // this.dashboardService.validateLogin(this.loginObj).subscribe((result) => {
    //   localStorage.set("loginInfo", result);
    //   this.router.navigate(['/dashboard']);
    // })
  }

}
