import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({headers: request.headers.set('acces-token', '12345')});
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error && error.status) {
          if (error.status === 401 || error.status === 2002) {
              alert('Login expires');
              localStorage.removeItem('currentUser');
              localStorage.removeItem('remember');
              localStorage.removeItem('userID');
              window.location.href = '/login';
            // Show no automatic flash message for 404 code and 2002 -- which is our own mapping of 404
          } else {
            //
          }
        }
        return throwError(error);
      })
    );
  }
}
